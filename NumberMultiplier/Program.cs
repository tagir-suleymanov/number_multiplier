﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberMultiplier
{
    class Program
    {
        static void Main(string[] args)
        {
            Start:
                Random rnd = new Random();
                int first = rnd.Next(1, 11);
                int second = rnd.Next(1, 11);
                int result = first * second;
                Console.WriteLine(first + " times " + second + " will be:");
                int input = Convert.ToInt32(Console.ReadLine());
                if (result == input)
                {
                    Console.WriteLine("Awesome, you`re right!");
                }
                else
                {
                    Console.WriteLine("You`re wrong, it`s not " + input + ", it`s " + result + ". Try again :)");
                }
                goto Start;
        }
    }
}
